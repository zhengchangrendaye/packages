# This file mark some packages with some different things

This is a file mark package(s) building abnornmal.

Some packages can't build as normal with default option(s) as offical PKGBUILD files, with other package can't pass test process while building with PKGBUILD file or source code.

## Package PKGBUILD file different from offical

### Core Repository

- `binutils`: disable cet, gold option, add '--disable-x86-used-note' option, change target to mips64r6el
- `glibc`: disable cet, multi-arch option
- `gcc`: disable cet, multi-lib option,change 'linker-hash-style' to both, take down some library that mips unsupoort at now: libitm, libsanitizer, libvtv, libquadmath
- `aduit`: remove `linux-headers` depend
- `pacman`: remove `fakechroot` check depend
- `systemd`: remove `gnu-efi-libs` depend, and disable it's option
- `python`: remove some depend
- `filesystem`: changed lib64 folder judge condition
- `glib2`: remove `gtk-doc` `libsysprof-capture` depend, disable related option; disable tests option, because it can't pass test now
- `glibc`: disable multiarch support
- `openssh`: remove linux-headers depend
- `openssl`: speical change configure cpu arch
- `libsecret`: remove `gjs` depend

### Other Repository

- `pixman`: disable some option
- `mkinitcpio-busybox`: delete arch and platform option
- `valgrind`: delete 32bit ABI depend and support
- `python`: disable some option
- `vim`: remove some depend
- `ruby`: remove some depend
- `python2`: remove some depend
- `cmake`: change configure to -no-qt-gui
- `llvm`: remove depend about docs and ocaml, don't build anything about docs
- `boost`: disable all option about ocaml and mpi
- `lld`: redefind compiler to clang but no gcc by changing source code's CMakeLists.txt
- `freetype2`: remove some depend, don't build freetype2-demo, take down some command
- `python-fonttools`: disable check and remove `check` depend, to prevent depend loop
- `harfbuzz`: disable graphlite, docs, chafa option manual
- `ffcall`: force change pre-compile assembly source code to pass building
- `gobject-introspection`: disable gtk-doc option
- `rust`: remove `rust-musl` `rust-wasm` package build because offical unsupport on mipsisa64r6. change config.toml's build target.
- `mesa`: remove `vulkan-intel` `vulkan-radeon` `vulkan-swrast` package build because unsupport on mips64r6. remove '-mtls-dialect=gnu' flag
- `wayland`: disable document option
- `rrdtool`: source tarball address code changed, so we update it manually to 1.8.0
- `yajl`: source tarball address code changed, so we fix it manually
- `libtheora`: specify build host manually
- `libepoxy`: remove doxygen depend
- `aom`: remove doxygen depend
- `lapack`: remove doxygen depend, and lapack-docs packgae-build
- `dbus`: remove doxygen depend
- `hdf5`: disabled all option aand depend about java
- `libxkbcommon`: disable document because remove doxygen depend
- `lensfun`: remove `doxygen` depend
- `hspell`: remove `qt6-webengine` and it's command
- `nuspell`: remove `pandoc` depend
- `rust`: build in speical way (with a different config.toml)
- `ding-libs`: remove `doxygen` and `check` depend
- `gssproxy`: remove `doxygen` depend
- `openjpeg2`: remove `doxygen` depend
- `libsigc++`: disble documention option because leak of `doxygen` depend
- `dav1d`: disble documention option because leak of `doxygen` depend
- `libimagequant`: add '--disable-sse' compile option
- `cppunit`: remove `doxygen` depend
- `python-matplotlib`: remove all optional depend
- `nss`: change target type to mips64
- `libteam`: remove `doxygen` depend
- `libbluray`: remove `java` depend, add '--disable-bdjava-jar' compile option
- `libsamplerate`: remove `fftw` depend
- `vmaf`: remove `doxygen` depend
- `fmt`: remove `doxygen` depend and npm plugin install
- `libssh`: remove `doxygen` depend
- `vid.stab`: remove sse2 compile option manually
- `libavif`: remove `rav1e` depend, turn off rav1e option
- `libheif`: remove `libde265` depend
- `v4l-utils`: remove `qt5-base` depend
- `graphite`: remove `doxygen` depend, and disable 'make docs' command for leak of it
- `liblo`: remove `doxygen` depend
- `libltc`: remove `doxygen` depend
- `libsrtp`: remove `doxygen` depend and disable doc option
- `gavl`: remove `doxygen` depend and add '--without-doxygen option' whe configure
- `modemmanager` & `ibmm-glib`: remove `polkit` depend and add change option 'polkit' to no
- `librsvg`: use the last version 2.40.20 unneed `rust` <!-- ababf709bade430b3bc182d25d9969f734fb2e1b -->
- `libical`: remove `doxygen` depend
- `librevenge`: remove `doxygen` depend
- `libwpd`: remove `doxygen` depend
- `libwpg`: remove `doxygen` depend
- `libodfgen`: remove `doxygen` depend
- `libvisio`: remove `doxygen` depend
- `libfreehand`: remove `doxygen` depend
- `libqxp`: remove `doxygen` depend
- `libmspub`: remove `doxygen` depend
- `libpagemaker`: remove `doxygen` depend
- `libcdr`: remove `doxygen` depend
- `ghostscript`: remove `gtk3` depend
- `cups`: remove `cups-filters` `colord` `avahi` depend
- `gtk2`: remove `gtk-update-icon-cache` depend
- `libopenraw`: use the last version 0.1.3-2 unneed `cargo` <!-- 6d4332452fa143641339f0ee53d79bbffaa33efd -->
- `imagemagick`: remove `libjxl` depend, delete libjxl's option
- `imlib2`: delete '--enable-amd64' option
- `lv2`: remove `doxygen` depend
- `polkit`: use the last version 0.1.3-2 unneed `js` <!-- 8ff3c3c9e8c80fea33d1d1a5aa3eea41edc59eb7 -->
- `subversion`: arhclinux's version is outdated, so we update it manually to 1.14.2; remove `kwallet` `kdelibs4support` `java` depend, delete option about java(hl) and '--with-kwallet'
- `fuse2`: build with glibc-2.32, because it has bug with higer version's glibc
- `python-wsaccel`: change check command's path to mips64
- `gf2x`: remove sse2 compile option manually
- `graphviz`: remove `mono` `ocaml` `qt5-base` depend
- `adios2`: remove `libfabric` depend
- `glibmm`: remove `doxygen` `glib-networking`depend, add 'build-documentatio=false' option, remove package documention command
- `libxml++`: remove `doxygen` depend, add 'build-documentatio=false' option, remove package documention command in package()
- `glfw`: remove `doxygen` depend
- `emacs`: remove `libgdk-3.so` `libgtk-3.so` `librsvg-2.so` `libgccjit` depend, and change option to '--without-native-compilation'
- `jack2`: remove `doxygen` depend, and change it's option to no
- `sord`: remove `doxygen` depend, and add option 'docs=disabled'
- `sratom`: remove `doxygen` depend, and add option 'docs=disabled'
- `sane`: remove `avahi` `poppler-glib` depend and it's compile option temporaily
- `appstream`: remove `libsoup` `qt5-tools` `librsvg` depend, disable 'compose' option for no suitable version librsvg
- `librabbitmq-c`: remove `doxygen` depend, and disable documention option
- `python-scikit-learn`: remove `openmp` depend
- `libinput`: remove `gtk4` `check` depend, and make 'debug-gui' 'tests' option false
- `poppler`: remove `qt5-base` `qt6-base` depend, and it's related packgae-build
- `thrift`: remove `qt5-base` depend and it's option
- `libsoup`: remove `libsysprof-capture` depend, and disable 'sysprof' option
- `glib-networking`: remove `libproxy` depend, and disable it's option
- `libnotify`: remove `gtk3` `gtk4` `qt5-base` depend, and disable it's option
- `samba`: remove `ceph-libs` `glusterfs` depend
- `libsoup3`: remove `libsysprof-capture` depend, and disable 'sysprof' option
- `gtk3`: remove `gtk-update-icon-cache` depend
- `avahi`: remove `doxygen` `qt5-base` depend, and disable qt5 option
- `primecount`: turn off 'WITH_FLOAT128' option
- `check`: remove `texlive-bin` `doxygen` depend, and disable docs make option
- `gap`: remove `c-xsc` depend
- `singular`: remove `doxygen` depend
- `liboggz`: remove `doxygen` depend
- `orcania`: remove `doxygen` depend, and disable docs option
- `yder`: remove `doxygen` depend, and disable docs option
- `doxygen`: remove `qt5-base` depend, and disable 'build_doc' 'build_wizard' option
- `ftjam`: change soure tarball address and version
- `cups-filters`: remove `mupdf-tools` depend, and add '--disable-mutool' option
- `pulseaudio`: remove `fftw` `gst-plugins-base-libs` depend, and add '-D fftw=disabled -D gstreamer=disabled -D bluez5-gstreamer=disabled' option
- `qpdf`: remove `texlive-bin` depend, and disable documention
- `libgme`: disable option 'ENABLE_UBSAN', for mips's platfrom have no libusan at now
- `ocaml`: add '--disable-native-compiler', delete '--enable-frame-pointers'
- `weston`: remove `freerdp` `pipewire` depend, add '-Dbackend-rdp=false' '-Dremoting=false' '-Dpipewire=false' option
- `texlive-bin`: remove `clisp` depend, disable JIT function by adding '--disable-luajittex --disable-luajithbtex --disable-mfluajit' option, disable xindy module by adding '--disable-xindy' option (because we have no clisp now)
- `ibus`: remove `gtk4` `qt5-base` depend, and remove gtk4 ui option
- `pipewire`: remove `gst-plugins-base` depend

<!-- - `libffdao`: remove `python-pyqt5` depend -->
<!-- - `atkmm`: remove `doxygen` depend, and add option 'build-documentation=false' -->
<!-- - `zbar`: remove `gtk3` `qt5-x11extras` depend -->
<!-- - `gtk4`: remove `gst-plugins-bad-libs` depend -->
<!-- - `cairomm`: remove `doxygen` depend, and add option 'build-documentation=false' -->
<!-- - `pangomm`: remove `doxygen` depend, and add option 'build-documentation=false' -->
<!-- clisp`: force change source from hg to tarball, because hg can't download success -->

### Aur Repository

## Package test fail while building with PKGBUILD file

### Core Repository

- `binutils`
- `glibc`
- `gcc`
- `coreutils`
- `systemd`
- `openssl`
- `sudo`
- `grep`
- `pacman`
- `gettext`
- `libsecret`: test timeout

### Other Repository

- `dbus-broker`
- `libevent`
- `ctag`
- `gtest`
- `pixman`: cause by timeout. maybe fix by turn on the mips-dsp option
- `perl`
- `perl-dbd-mariadb`
- `perl-dbd-mysql`
- `leatherman`
- `libdwarf`
- `opensp`
- `fuse2`
- `vim`
- `llvm`
- `clang`
- `llvm13`
- `clang13`
- `lld`
- `libtool`
- `python-cairo`: may be cause by not full support of harfbuzz.
- `ffcall`
- `valgrind`: can't identify mips64r6, so it doesn't process check
- `wayland`
- `libwacom`
- `python-snappy`
- `python-greenlet`: seems that test.py have some problems
- `libxkbcommon`: skiped 1 test
- `json-glib`: timeout 1 test
- `at-spi2-core`
- `gsl`
- `python-mpi4py`
- `python-pycapnp`
- `givaro`
- `python-pyzmq`
- `rcs`
- `ksh`
- `libjpeg-turbo`
- `dav1d`
- `python-cryptography`: cause by `cargo`/`rust`
- `python-pillow`
- `python-pandas`
- `libgudev`
- `hotdoc`
- `gdk-pixbuf2`
- `at-spi2-atk`
- `dconf`
- `gd`
- `xmlsec`
- `python-bcrypt`
- `libgit2-glib`
- `libsrtp`
- `libltc`
- `libgsf`
- `babl`
- `tbb`: test 23/118 will cause timeout 
- `gdk-pixbuf`: test 23/23 failed
- `tpm2-abrmd`: XFAIL: test/integration/start-auth-session.int
- `swtpm`: test timeout
- `python-dulwich`
- `python-wsaccel`
- `harfbuzz`: 1 timeout
- `colord`
- `libdazzle`: 6 failed
- `php`
- `python-wrapt`
- `php7`
- `elfutils`: 0.187: 11 FAILED
- `apparmor`: 'make -C parser check' and 'make PYFLAKES='/usr/bin/true' -C utils check' run failed
- `libmysofa`
- `grpc`
- `libsoup`
- `rest`
- `libsoup3`
- `memcached`
- `symengine`
- `linbox`
- `perl-tk`: we have no x11 at now
- `glib-perl`
- `autogen`
- `doxygen`
- `pulseaudio`
- `git`
- `gtksourceview3`
- `wireplumber`: qemu aborted

## Package test fail while building with source code

## Package built in dirty-way so need to repack in the future

```
gobject-introspection
dav1d
rust
libdazzle
```
<!-- sysprof libsysprof-capture -->

## Package build fail that need to fix upstream source code

- `ffcall`: need to update it's pre-compile's gcc version, and add '-mno-branch-likely' option (temporaily built success by fix it manually, need to feedback upstream)
- `libjpeg-turbo`: compiled failed caused by assembly part about mips and loongson fixed (temporaily built success by FlyGoats's patch, waiting for upstream accepting FlyGoats's PR)
- `lldb`: need to transplant some code
- `openmp`: mipsr6 define may have some problem
- `valgrind`: configure & make file have unnecessary "-march=mips64r2" tag option with mips (temporaily built success by fix it manually, need to feedback upstream)
- `nodejs`: unsupport mips r6 now
- `openblas`: compiled failed caused by assembly part about mips and loongson fixed, and have unnecessary "-march=mips64" tag option with mips (waiting for upstream accepting FlyGoats's PR)
- `aalib`: config.guess type error (temporaily built success by fix it manually)
- `dsniff`: had a type error in 'pcaputil.h', need to fix by upstream
- `libmad`: some problem with assembly command in .h file
- `sbcl`: mipsel-binfmt-P: Could not open '/lib/ld.so.1'`: No such file or directory
- `gfan`: because header file of cddlib moved, it reported no such header files. need to fix by upstream
- `c-xsc`: unknown error. ../../src/interval.inl:136:58: error: ISO C++17 does not allow dynamic exception specifications
- `a52dec`:  config.guess type error (temporaily built success by fix it manually)
- `zn_poly`: some problem with assembly command in .h file
- `libfabric`: only support x86 platform. ./prov/opx/include/rdma/opx/fi_opx_timer.h:66:2: error: #error "Cycle timer not defined for this platform"
- `rav1e`: cause by rust
- `mono`: still unsupport 64-bit on mips. need to transplant
- `devil`: have a conflict variable 'mips' with gcc (temporaily built success by fix it manually)
- `avisynthplus`: need to transplant. unsupport mips at all.
- `fftw`: compile option problems
- `clisp`: assembly part about mips unsupport r6
- `chmlib`: leak of some include header file
- `SVT-HEVC`: use x86's header file
- `c-xsc`: unknown error. Aiksaurus.cpp:101:17: error: ISO C++17 does not allow dynamic exception specifications
- `arrow`: only support x86
- `pari`: assembly part about mips unsupport r6
- `libgringotts`: config.guess type error (temporaily built success by fix it manually)
- `mupdf`: binutils ld moudle have some proble on mips platform, so it can't link resource file to library  

## Package build fail with other reasons

- `xfsprogs`: built packages have some install problem
